import random
import pandas as pd
import numpy as np

from pathlib import Path


TRAIN_PATH = Path("data/raw/train.csv")
TEST_PATH = Path("data/raw/test.csv")


train = pd.read_csv(TRAIN_PATH)
test = pd.read_csv(TEST_PATH)


def foonc(df):
    age_mean = int(train["Age"].mean())
    age_std = int(train["Age"].std())
    len_value = df["Age"].isnull().sum()

    t_val = []
    for i in range(0, len_value):
        spisok = random.randint(age_mean - age_std, age_mean + age_std)
        t_val.append(spisok)
    return t_val


# заполнение пропущенных значений
test_value = foonc(test)
test["Age"][np.isnan(test["Age"])] = test_value

train_value = foonc(train)
train["Age"][np.isnan(train["Age"])] = train_value
